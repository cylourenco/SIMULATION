#pragma once

#include <msclr\marshal_cppstd.h>
#include <sstream>
#include "simul.h"

namespace ConsoleApplication1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	using namespace msclr::interop;

	/// <summary>
	/// Description r�sum�e de form_main
	/// </summary>
	public ref class form_main : public System::Windows::Forms::Form
	{
	public:
		form_main(void)
		{
			InitializeComponent();
			//
			//TODO: ajoutez ici le code du constructeur
			//
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~form_main()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:











	private: System::Windows::Forms::TextBox^ textBox5;
	private: System::Windows::Forms::TextBox^ textBox6;
	private: System::Windows::Forms::TextBox^ textBox7;
	private: System::Windows::Forms::Label^ Duree;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::Label^ label7;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::RichTextBox^ richTextBox2;









	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->Duree = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->richTextBox2 = (gcnew System::Windows::Forms::RichTextBox());
			this->SuspendLayout();
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(393, 189);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(198, 20);
			this->textBox5->TabIndex = 13;
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(393, 235);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(198, 20);
			this->textBox6->TabIndex = 14;
			// 
			// textBox7
			// 
			this->textBox7->Location = System::Drawing::Point(393, 281);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(198, 20);
			this->textBox7->TabIndex = 15;
			// 
			// Duree
			// 
			this->Duree->AutoSize = true;
			this->Duree->Location = System::Drawing::Point(216, 189);
			this->Duree->Name = L"Duree";
			this->Duree->Size = System::Drawing::Size(120, 13);
			this->Duree->TabIndex = 16;
			this->Duree->Text = L"Dur�e de la simulation : ";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(205, 235);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(131, 13);
			this->label6->TabIndex = 17;
			this->label6->Text = L"Dur�e entre deux clients : ";
			this->label6->Click += gcnew System::EventHandler(this, &form_main::label6_Click);
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(176, 281);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(160, 13);
			this->label7->TabIndex = 18;
			this->label7->Text = L"Dur�e de traitement d\'un client : ";
			this->label7->Click += gcnew System::EventHandler(this, &form_main::label7_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(30, 23);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(561, 131);
			this->button3->TabIndex = 19;
			this->button3->Text = L"SIMULER";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &form_main::button3_Click);
			// 
			// richTextBox2
			// 
			this->richTextBox2->Location = System::Drawing::Point(661, 12);
			this->richTextBox2->Name = L"richTextBox2";
			this->richTextBox2->Size = System::Drawing::Size(497, 316);
			this->richTextBox2->TabIndex = 20;
			this->richTextBox2->Text = L"";
			this->richTextBox2->TextChanged += gcnew System::EventHandler(this, &form_main::richTextBox2_TextChanged);
			// 
			// form_main
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1208, 701);
			this->Controls->Add(this->textBox5);
			this->Controls->Add(this->richTextBox2);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->Duree);
			this->Controls->Add(this->textBox7);
			this->Controls->Add(this->textBox6);
			this->Name = L"form_main";
			this->Text = L"form_main";
			this->Load += gcnew System::EventHandler(this, &form_main::form_main_Load);
			this->Click += gcnew System::EventHandler(this, &form_main::form_main_Click);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	//private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {


	//	System::String^ s_nb_1 = textBox1->Text;
	//	System::String^ s_nb_2 = textBox2->Text;

	//	msclr::interop::marshal_context context;
	//	std::string chaine1 = context.marshal_as<std::string>(s_nb_1);
	//	std::string chaine2 = context.marshal_as<std::string>(s_nb_2);


	//	// autre sol
	//	int nb1 = -1;
	//	int nb2 = -1;
	//	std::istringstream(chaine1) >> nb1;
	//	std::istringstream(chaine2) >> nb2;
	//	int nb_res = nb1 + nb2;


	//	// le res. en string
	//	std::stringstream chaine_res;
	//	chaine_res << nb_res;
	//	std::string chaine_res_string = chaine_res.str();

	//	System::String^ s_nb_res = marshal_as<String^>(chaine_res_string);

	//	textBox3->Text = s_nb_res;




	//}
	private: System::Void form_main_Load(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {

	}
	private: System::Void form_main_Click(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void label6_Click(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void label7_Click(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void pictureBox1_Click(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void richTextBox2_TextChanged(System::Object^ sender, System::EventArgs^ e) {

	}
	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) {
		System::String^ s_nb_1 = textBox5->Text;
		System::String^ s_nb_2 = textBox6->Text;
		System::String^ s_nb_3 = textBox7->Text;

		msclr::interop::marshal_context context;
		std::string chaine1 = context.marshal_as<std::string>(s_nb_1);
		std::string chaine2 = context.marshal_as<std::string>(s_nb_2);
		std::string chaine3 = context.marshal_as<std::string>(s_nb_3);


		// autre sol
		int nb1 = -1;
		int nb2 = -1;
		int nb3 = -1;
		std::istringstream(chaine1) >> nb1;
		std::istringstream(chaine2) >> nb2;
		std::istringstream(chaine3) >> nb3;

		exec_simulation(nb1, nb2, nb3, richTextBox2);

		
	}
	private: System::Void chart1_Click(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void button2_Click_1(System::Object^ sender, System::EventArgs^ e) {
		//chart1->Series["Series1"]->Points->AddXY(10, 20);
/*
		float x = 0.0;
		while (x <= 6.28)
		{
			float y = cos(x);
			chart1->Series["Series1"]->Points->AddXY(x, y);
			x = x + 0.1;
		}
	*/
	}

private: System::Void panel1_Paint(System::Object^ sender, System::Windows::Forms::PaintEventArgs^ e) {
}
};
}
