#ifndef SIMUL
#define SIMUL

#include<stdlib.h>
using namespace System;
using namespace System::Windows::Forms;

const int taille = 10;

typedef struct T_piece
{
	int numero;
	int date_entree;
	int date_sortie;
}T_piece;

typedef struct T_machine
{
	int etat;
	int duree_traitement;
	int DPE;
	T_piece contenu;
}T_machine;

typedef struct T_file
{
	int d;
	int f;
	T_piece liste[taille + 1];
}T_file;

typedef struct T_entree
{
	int etat;
	int tag_prec;
	int DPE;
}T_entree;

typedef struct T_sortie
{
	T_piece liste[99999];
	int nb;
}T_sortie;

void deposer_piece_file(T_file& F, T_piece P);

T_piece retirer_piece_file(T_file& F, T_piece P);

void init_file_vide(T_file& F);

void exec_simulation(int duree_simul, int duree_inter, int duree_trait, System::Windows::Forms::RichTextBox^ zone);

#endif 

