#include "simul.h"
#include <iostream>

void deposer_piece_file(T_file& F, T_piece P)
{
	if (F.f % taille + 1 != F.d)
	{
		F.liste[F.f] = P;
		F.f = (F.f % taille) + 1;
	}
	else
	{
		exit(-1);
	}
}

T_piece retirer_piece_file(T_file& F, T_piece P)
{
	if (F.d != F.f)
	{
		P = F.liste[F.d];
		F.d = (F.d + 1) % (taille + 1);
	}
	else
	{
		exit(-1);
	}

	return P;
}

void init_file_vide(T_file& F)
{
	F.d = 0;
	F.f = 0;
}

void deposer_piece_sur_la_sortie(T_sortie& S, T_piece P)
{
	S.nb++;
	S.liste[S.nb] = P;
}

void retirer_piece_sur_la_machine(T_machine& M, T_piece P)
{
	P = M.contenu;
	M.etat = 0; // machine vide
	M.DPE = 999;
}

void deposer_piece_sur_la_machine(T_machine& M, T_piece P, int date_simul)
{
	M.contenu = P;
	M.etat = 1; // machine occup�e
	M.DPE = date_simul + M.duree_traitement;
}

void exec_simulation(int duree_simul, int duree_inter, int duree_trait, System::Windows::Forms::RichTextBox^ zone)
{
	zone->Text = "Je suis";
	zone->Refresh();
	T_machine M;
	T_entree E;
	T_sortie S;
	S.nb = 0;

	T_file ma_file;
	init_file_vide(ma_file);

	T_piece une_piece;
	une_piece.numero = 101;
	deposer_piece_file(ma_file, une_piece);

	une_piece.numero = 102;
	deposer_piece_file(ma_file, une_piece);

	une_piece = retirer_piece_file(ma_file, une_piece);

	std::cout << une_piece.numero << std::endl;

	int date_simul = 0;
	int res = -1;
	while (date_simul <= duree_simul)
	{
		
		if (M.DPE < E.DPE)
			res = 1;

		if (res == -1) // traiter l'entree 
			date_simul = E.DPE;

		if (res == 1) // traiter la machine
		{
			date_simul = M.DPE;
			T_piece P = M.contenu;
			
			//poser P sur la sortie
			retirer_piece_sur_la_machine(M, P);
			deposer_piece_sur_la_sortie(S, P);

			// tester la file
			if (ma_file.d != ma_file.f) // file vide
			{
			}
			else
			{
				P = retirer_piece_file(ma_file, P);
				deposer_piece_sur_la_machine(M, P, date_simul);
				if (E.etat = 3)
				{
					E.DPE = date_simul;
					E.etat = 2; 
				}
			}

		}
	 }

}