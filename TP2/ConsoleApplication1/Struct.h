#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include <msclr/marshal_cppstd.h>
using namespace System::Windows::Forms;
using namespace System::Windows::Forms::DataVisualization::Charting;


using namespace std;

const int TAILLE_FILE = 6; // Taille maximale de la file
const int MAX_TAB = 1000; // Taille maximale de la file
const int INF = 99999; // Taille maximale de la file



typedef struct T_piece{
	int id;
	vector<int> tab_machines[30];  // historique des machines dans lesquelles est passee la piece (pas entree dans la file mais dans la machine)
	int index_tab = 1;
}T_piece;


typedef struct T_machine {
	int duree_Traitement;
	T_piece contenu;
	int dpe;
	int etat;   // 0 = vide (pas de DPE) 1 = occupe 2 = bloque 
	// pour la machine A : 3 = bloque a cause de B et 4 = bloque a cause de C 
}T_machine;


typedef struct T_entree {
	int duree_interClient; 
	int dpe;
	T_piece suivant;
	int etat; // 0 ou 2
	int id;
}T_entree;


typedef struct T_sortie {
	T_piece traite[MAX_TAB];
	int nb_traite;
}T_sortie;

typedef struct T_file {
	int debut;            // Indice de debut de la file
	int fin;              // Indice de fin de la file
	T_piece elements[TAILLE_FILE]; // Tableau d'entiers pour stocker les elements de la file
}T_file;


bool isEmpty(T_file& f);

bool isFull(T_file& f);

void enfiler(T_file& f, T_piece item);

T_piece defiler(T_file& f);

void init_file(T_file& f);