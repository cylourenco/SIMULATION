#ifndef SIMUL
#define SIMUL

#include <random>
#include "Struct.h"

#define p 0.4
#define q 0.4


void simuler(int duree, System::Windows::Forms::RichTextBox^ zone, System::Windows::Forms::DataVisualization::Charting::Chart^ graphe1);

void initElement(T_machine& MA, T_machine& MB, T_machine& MC, T_sortie& S, T_entree& E);

int probabilite(float f);

void deposer_piece_dans_la_file(T_file &f, T_piece piece);
void deposer_piece_dans_la_machine(T_file& f, T_machine& M, int numMachine, int DS);
void machine_vers_machine(T_machine& M1, T_file& f1, T_machine& M2, T_file& f2, int numMachine1, int numMachine2, int DS);
void machine_vers_sortie(T_machine& M, T_file& f, int numMachine, int DS, T_sortie& S);

T_piece retirer_piece_de_la_file(T_file& f);

int prochain_DPE(T_machine MA, T_machine MB, T_machine MC, T_entree ME);

#endif