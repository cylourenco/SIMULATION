#include "Simul.h"

void simuler(int duree, System::Windows::Forms::RichTextBox^ zone, System::Windows::Forms::DataVisualization::Charting::Chart^ graphe1) {

	srand(time(NULL));

	T_machine MA, MB, MC;
	T_piece P{};
	T_entree E;
	T_sortie S;

	initElement(MA, MB, MC, S, E);

	T_file A_file;
	init_file(A_file);
	T_file B_file;
	init_file(B_file);
	T_file C_file;
	init_file(C_file);

	// DS = duree simulation
	int DS = 0;
	int prochain = 0; // numero de la prochaine machine ou autre


	while (DS < duree)
	{
		prochain = prochain_DPE(MA, MB, MC, E);

		switch (prochain)
		{
		case 0: // evenement entree d'une nouvelle piece dans le syst�me
			DS = E.dpe; // saut a la date du prochain evenement

			// on verifie letat de lentree
			// 1 cas : entree bloquee
			if (E.etat == 2) {
				// une place a ete liberee dans la file de A
				E.etat = 0; // pas de traitement pour l'entree
				E.suivant.tab_machines[0][1] = DS; // maj de la date d'entree dans le systeme
				E.dpe += E.duree_interClient; // temps avant que la prochaine piece ne puisse arriver

				deposer_piece_dans_la_file(A_file, E.suivant);
			}
			else { // 1 (evenement standard)
				E.suivant.id = E.id; // id unique par piece
				E.id++;

				E.suivant.tab_machines[0][1] = DS; // DS = date entree systeme

				//test A pleine
				if (isFull(A_file)) {
					E.etat = 2;
					E.dpe = INF;
				}
				else {
					if (MA.etat == 0) {
						MA.etat = 1;
						MA.contenu = E.suivant;
						MA.dpe = MA.duree_Traitement + DS;
						MA.contenu.tab_machines[MA.contenu.index_tab].push_back(1);
						MA.contenu.tab_machines[MA.contenu.index_tab].push_back(DS);
					}
					else {
						deposer_piece_dans_la_file(A_file, E.suivant);
					}
					// maj du dpe de l'entree
					E.dpe = DS + E.duree_interClient;
				}
			}
			break;

		case 1: // A
			DS = MA.dpe;

			// Machine A bloquee (la piece ne peut pas etre bloquee)
			if (MA.etat == 3 || MA.etat == 4) {

				// une place a ete liberee dans B ou C
				if (MA.etat == 3) { // dans B
					deposer_piece_dans_la_file(B_file, MA.contenu);
				}
				else { // dans C
					deposer_piece_dans_la_file(C_file, MA.contenu);
				}

				MA.etat = 0;
				MA.dpe = INF;

				// file A -> machine A
				if (!isEmpty(A_file)) {
					deposer_piece_dans_la_machine(A_file, MA, 1, DS);
				}
			}
			else { // evenement standard
				if (probabilite(q) == 1)
				{
					// on va vers B
					machine_vers_machine(MA, A_file, MB, B_file, 1, 2, DS);

				}
				else {
					// on va vers C
					machine_vers_machine(MA, A_file, MC, C_file, 1, 3, DS);
				}
			}

			// Verification Liberation de places sur la file A
			if (!isFull(A_file)) {
				// on regarde si des machines etaient en etat bloque a cause de A
				// -> pour les debloquer

				// attention aux priorites
				if (MB.etat == 2) { // on choisit B en premier (meme titre que C)
					MB.dpe = DS;
				}
				else if (MC.etat == 2) {
					MC.dpe = DS;
				}
				else if (E.etat == 2) { // la priorite est donnee aux pieces deja dans le systeme
					E.dpe = DS;
				}

			}
			break;

		case 2: // B
			DS = MB.dpe;

			// Machine B bloquee (la piece ne peut pas etre bloquee)
			if (MB.etat == 2) {

				// une place a ete liberee dans A
				deposer_piece_dans_la_file(A_file, MB.contenu);

				MB.etat = 0;
				MB.dpe = INF;

				// file B -> machine B
				if (!isEmpty(B_file)) {
					deposer_piece_dans_la_machine(B_file, MB, 2, DS);
				}
			}
			else { // evenement standard
				if (probabilite((1 - p)) == 1)
				{
					// on va vers la machine A
					machine_vers_machine(MB, B_file, MA, A_file, 2, 1, DS);

				}
				else {
					// on sort
					machine_vers_sortie(MB, B_file, 2, DS, S);
				}
			}

			// Verification Liberation de places sur la file A
			if (!isFull(B_file) && MA.etat == 3) {
				// on regarde si des machines etaient en etat bloque a cause de A
				// -> pour les debloquer
				MA.dpe = DS;

			}
			break;
		case 3: // C
			DS = MC.dpe;

			// Machine C bloquee (la piece ne peut pas etre bloquee)
			if (MC.etat == 2) {

				// une place a ete liberee dans A
				deposer_piece_dans_la_file(A_file, MC.contenu);

				MC.etat = 0;
				MC.dpe = INF;

				// file C -> machine C
				if (!isEmpty(C_file)) {
					deposer_piece_dans_la_machine(C_file, MC, 3, DS);
				}
			}
			else { // evenement standard
				if (probabilite((1 - p)) == 1)
				{
					// on va vers la machine A
					machine_vers_machine(MC, C_file, MA, A_file, 3, 1, DS);

				}
				else {
					// on sort
					machine_vers_sortie(MC, C_file, 3, DS, S);
				}
			}

			// Verification Liberation de places sur la file A
			if (!isFull(C_file) && MA.etat == 4) {
				// on regarde si des machines etaient en etat bloque a cause de A
				// -> pour les debloquer
				MA.dpe = DS;

			}
			break;

		default: // toutes les machines sont en etats bloquees (= tous les dpe en INF)
			zone->AppendText("La simulation est bloquee à la date : " + DS + "\n\n");

			DS = duree; // on sort du while

			break;
		}
	}


	// affichage des textes et graphiques du traitement des pieces
	// qu'on a enregistré tout au long (dans tab_machines)

	for (int i = 0; i < S.nb_traite; i++) {
		
		std::ostringstream oss;
		zone->AppendText("\nTrace de la pièce " + S.traite[i].id + "  |  entree systeme : " + S.traite[i].tab_machines[0][1] + "   sortie systeme : " + S.traite[i].tab_machines[0][2] + "\n");
		
		for (int j = 1; j < S.traite[i].index_tab; j++)
		{
			oss << "machine : " << S.traite[i].tab_machines[j][0]
				<< ", DateE: " << S.traite[i].tab_machines[j][1]
				<< ", DateS: " << S.traite[i].tab_machines[j][2] << endl ;
		}

		std::string result = oss.str();
		System::String^ resultCli = msclr::interop::marshal_as<System::String^>(result);
		zone->AppendText(resultCli);

		graphe1->Series["Series1"]->Points->AddXY(S.traite[i].tab_machines[0][1], S.traite[i].tab_machines[0][2]);

		oss.clear();
		oss.str("");
	}

	graphe1->ChartAreas[0]->AxisX->Title = "Temps";
	graphe1->ChartAreas[0]->AxisY->Title = "Nombre de pieces";

	graphe1->ChartAreas[0]->AxisX->MajorGrid->Enabled = false;
	graphe1->ChartAreas[0]->AxisY->MajorGrid->Enabled = false;

	graphe1->Series["Series1"]->ChartType = SeriesChartType::Point;
	graphe1->Invalidate();


	Application::DoEvents();

}

T_piece retirer_piece_de_la_file(T_file& f) {
	return defiler(f);
}


void deposer_piece_dans_la_file(T_file &f, T_piece piece) {
	// piece qui sort d'une machine (ou entree) pour aller dans la file suivante
	enfiler(f, piece);
}

void deposer_piece_dans_la_machine(T_file& f, T_machine& M, int numMachine, int DS) {
	M.contenu = retirer_piece_de_la_file(f);

	M.contenu.tab_machines[M.contenu.index_tab].push_back(numMachine);
	M.contenu.tab_machines[M.contenu.index_tab].push_back(DS);

	M.dpe = DS + M.duree_Traitement;
	M.etat = 1; // machine occupee
}

void machine_vers_machine(T_machine& M1, T_file& f1, T_machine& M2, T_file& f2, int numMachine1, int numMachine2, int DS) {
	// maj tableau historique -> dans tous les cas on sort de la machine M1 (que la suivante soit bloquée ou non)
	M1.contenu.tab_machines[M1.contenu.index_tab].push_back(DS); // sortie de la machine M1
	M1.contenu.index_tab++;

	// on verifie la prochaine file/machine
	
	//test machine suivante pleine
	if (isFull(f2)) {
		if (numMachine2 == 2 || numMachine2 == 3)
			M1.etat = numMachine2 + 1; // 3 pour B, 4 pour C
		else
			M1.etat = 2;

		M1.dpe = INF;
	}
	else {
		
		if (M2.etat == 0) {
			M2.etat = 1;
			M2.contenu = M1.contenu;
			M2.dpe = M2.duree_Traitement + DS;
			M2.contenu.tab_machines[M2.contenu.index_tab].push_back(numMachine2);
			M2.contenu.tab_machines[M2.contenu.index_tab].push_back(DS);
		}
		else {
			deposer_piece_dans_la_file(f2, M1.contenu);
		}
		M1.etat = 0;
		M1.dpe = INF;

		// on s'occupe de la machine que l'on vient de liberer
		if (!isEmpty(f1)) {
			deposer_piece_dans_la_machine(f1, M1, numMachine1, DS);
		}
	}
}

void machine_vers_sortie(T_machine& M, T_file& f, int numMachine, int DS, T_sortie& S) {
	// maj tableau historique
	M.contenu.tab_machines[M.contenu.index_tab].push_back(DS); // sortie de la machine M
	M.contenu.index_tab++;

	// on ajoute la date de sortie du systeme sur la 1ere ligne
	M.contenu.tab_machines[0].push_back(DS);

	S.traite[S.nb_traite] = M.contenu;
	S.nb_traite++;

	M.etat = 0;
	M.dpe = INF;

	// on s'occupe de la machine que l'on vient de lib�rer
	if (!isEmpty(f)) {
		deposer_piece_dans_la_machine(f, M, numMachine, DS);
	}
}

void initElement(T_machine& MA, T_machine& MB, T_machine& MC, T_sortie& S, T_entree& E)
{
	MA.duree_Traitement = 8;
	MA.etat = 0;
	MA.dpe = INF;

	MB.duree_Traitement = 14;
	MB.etat = 0;
	MB.dpe = INF;

	MC.duree_Traitement = 12;
	MC.etat = 0;
	MC.dpe = INF;

	S.nb_traite = 0;

	E.duree_interClient = 3;
	E.etat = 0;
	E.dpe = 0;
	E.id = 1;
	E.suivant.tab_machines[0].push_back(0);
	E.suivant.tab_machines[0].push_back(0);

}

int probabilite(float f)
{
	float nb = static_cast<float>(rand())/RAND_MAX;
	int retour = 0;

	if (nb < f)
		retour = 1;
	
	return retour;
}


// verification de l'etat de la prochaine machine
// P a enregistre precedemment la machine ou elle veut aller ensuite
int prochain_DPE(T_machine MA, T_machine MB, T_machine MC, T_entree ME)
{
	int min_dpe = MA.dpe;
	int retour = 1;
	//  0 = Entree 1 = A 2 = B 3 = C
	if (min_dpe > MB.dpe) {
		retour = 2;
		min_dpe = MB.dpe;
	}
	if (min_dpe > MC.dpe) {
		retour = 3;
		min_dpe = MC.dpe;
	}
	if (min_dpe > ME.dpe) {
		retour = 0;
		min_dpe = ME.dpe;
	}
	if (min_dpe == INF)
		retour = -1;
	return retour;
}



