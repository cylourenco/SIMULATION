#ifndef SIMUL
#define SIMUL

#include <random>
#include "Struct.h"

#define p 0.6
#define q 0.4


void simuler(int duree, System::Windows::Forms::RichTextBox^ zone, System::Windows::Forms::DataVisualization::Charting::Chart^ graphe1);

void initElement(T_machine& MA, T_machine& MB, T_assembleur& A, T_entree& E1, T_entree& E2);

void deposer_piece_dans_la_file(T_file &f, T_piece piece);
void deposer_piece_dans_la_machine(T_file& f, T_machine& M, int numMachine, int DS);
void deposer_piece_dans_assembleur(T_assembleur& A, T_machine& M, int DS, int numMachine, T_file& f);
void assembleur_vers_sortie(T_assembleur& A, int DS, T_sortie& S);

T_piece retirer_piece_de_la_file(T_file& f);

int prochain_DPE(T_machine MA, T_machine MB, T_assembleur A, T_entree E1, T_entree E2);
void evenement_entree(int DS, T_entree& E, T_file& f, T_machine& M, int numEntree);

#endif