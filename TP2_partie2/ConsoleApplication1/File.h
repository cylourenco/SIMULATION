#pragma once

#include "struct.h"


const int p = 0.6;

typedef struct T_file {
    int debut;            // Indice de d�but de la file
    int fin;              // Indice de fin de la file
    T_piece elements[TAILLE_FILE]; // Tableau d'entiers pour stocker les �l�ments de la file
}T_file;

bool isEmpty(T_file &f);

bool isFull(T_file &f);

void enfiler(T_file &f, T_piece item);

T_piece defiler(T_file &f);

void init_file(T_file& f);




