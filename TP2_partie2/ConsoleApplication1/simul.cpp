#include "Simul.h"
 

void simuler(int duree, System::Windows::Forms::RichTextBox^ zone, System::Windows::Forms::DataVisualization::Charting::Chart^ graphe1) {

	srand(time(NULL));

	T_machine MA, MB;
	T_assembleur A;
	T_piece P{};
	T_entree E1;
	T_entree E2;
	T_sortie S;
	
	initElement(MA, MB, A, E1, E2);

	T_file A_file;
	init_file(A_file);
	T_file B_file;
	init_file(B_file);

	// DS = duree simulation
	int DS = 0;
	int prochain = 0;


	while (DS < duree)
	{
		prochain = prochain_DPE(MA, MB, A, E1, E2);

		switch (prochain)
		{
		case 0: // E1
			evenement_entree(DS, E1, A_file, MA, 0);		
			break;
		case 1: // E2
			evenement_entree(DS, E2, B_file, MB, 1);
			break;

		case 2: // A
			DS = MA.dpe;

			if (MA.etat == 2) // si l'état était bloqué (historique déjà complété)
				deposer_piece_dans_assembleur(A, MA, DS, 2, A_file);
			else { // sinon il faut remplir le tableau des historiques de pièces
				MA.contenu.tab_machines[MA.contenu.index_tab].push_back(DS); // sortie de la machine M
				MA.contenu.index_tab++;
				deposer_piece_dans_assembleur(A, MA, DS, 2, A_file);
			}

			// Verification Liberation de places sur la file A
			if (!isFull(A_file))
				// on regarde si l'entree 1 etait en etat bloque a cause de A
				if (E1.etat == 2)
					E1.dpe = DS;
			break;

		case 3: // B

			DS = MB.dpe;

			if (MB.etat == 2) // B était bloqué donc historique déjà rempli
				deposer_piece_dans_assembleur(A, MB, DS, 3, B_file);
			else {
				// maj tableau historique
				MB.contenu.tab_machines[MB.contenu.index_tab].push_back(DS); // sortie de la machine M
				MB.contenu.index_tab++;
				deposer_piece_dans_assembleur(A, MB, DS, 3, B_file);
			}


			// Verification Liberation de places sur la file B
			if (!isFull(B_file)) {
				// on regarde si l'entree 2 etait en etat bloque a cause de B

				if (E2.etat == 2) {
					E2.dpe = DS;
				}
			}
			break;
		case 4: // assembleur
			DS = A.dpe;

			assembleur_vers_sortie(A, DS, S); // seul choix

			// Verification Liberation de places sur la file A et B
			if (((A.etat == 2) || (A.etat == 0)) && (MB.etat == 2)) 
				MB.dpe = DS;
			if (((A.etat == 3) || (A.etat == 0)) && (MA.etat == 2))
				MA.dpe = DS;
			break;

		default: // toutes les machines sont en etats bloquees (= tous les dpe en INF)
			zone->AppendText("La simulation est bloquee à la date : " + DS + "\n\n");

			DS = duree; // on sort du while

			break;
		}
	}

	// affichage du traitement des pièces dans le système
	for (int i = 0; i < S.nb_traite; i++) {
		
		std::ostringstream oss;
		zone->AppendText("\nTrace de la pièce " + S.traite[i].id + "  |  entree systeme : " + S.traite[i].tab_machines[0][1] + "   sortie systeme : " + S.traite[i].tab_machines[0][2] + "\n");
		
		for (int j = 1; j < S.traite[i].index_tab; j++)
		{
			oss << "machine : " << S.traite[i].tab_machines[j][0]
				<< ", DateE: " << S.traite[i].tab_machines[j][1]
				<< ", DateS: " << S.traite[i].tab_machines[j][2] << endl ;
		}

		std::string result = oss.str();
		System::String^ resultCli = msclr::interop::marshal_as<System::String^>(result);
		zone->AppendText(resultCli);

		graphe1->Series["Series1"]->Points->AddXY(S.traite[i].tab_machines[0][1], S.traite[i].tab_machines[0][2]);

		oss.clear();
		oss.str("");
	}

	graphe1->ChartAreas[0]->AxisX->Title = "Temps";
	graphe1->ChartAreas[0]->AxisY->Title = "Nombre de pi�ces";

	graphe1->ChartAreas[0]->AxisX->MajorGrid->Enabled = false;
	graphe1->ChartAreas[0]->AxisY->MajorGrid->Enabled = false;

	graphe1->Series["Series1"]->ChartType = SeriesChartType::Point;
	graphe1->Invalidate();


	Application::DoEvents();

}

T_piece retirer_piece_de_la_file(T_file& f) {
	return defiler(f);
}


void deposer_piece_dans_la_file(T_file &f, T_piece piece) {
	// piece qui sort d'une machine (ou entree) pour aller dans la file suivante
	enfiler(f, piece);
}

void deposer_piece_dans_la_machine(T_file& f, T_machine& M, int numMachine, int DS) {
	M.contenu = retirer_piece_de_la_file(f);

	M.contenu.tab_machines[M.contenu.index_tab].push_back(numMachine);
	M.contenu.tab_machines[M.contenu.index_tab].push_back(DS);

	M.dpe = DS + M.duree_Traitement;
	M.etat = 1; // machine occupee
}


void deposer_piece_dans_assembleur(T_assembleur& A, T_machine& M, int DS, int numMachine, T_file& f) {
	// pour la distribution de la durée dans l'assembleur
	std::random_device rd;
	std::mt19937 gen(rd());
	std::exponential_distribution<> exp_dist(1.0 / 10.0);


	// test assembleur plein d'une piece venant de cette machine
	if (A.etat == numMachine || A.etat == 4) { // 4 = les deux pieces sont deja la
		M.etat = 2;
		M.dpe = INF;
	}
	else { // on peut ajouter la pièce

		if (A.etat == 0) { // assembleur vide
			
			A.etat = numMachine;

			if (A.etat == 1) { // On vient d'ajouter une pièce 1 à la machine 
				A.piece1 = M.contenu;
				A.piece1.tab_machines[A.piece1.index_tab].push_back(4); // 4 = numero de l'assembleur
				A.piece1.tab_machines[A.piece1.index_tab].push_back(DS);
			}
			else { // On vient d'ajouter une pièce 2 à la machine 
				A.piece2 = M.contenu;
				A.piece2.tab_machines[A.piece2.index_tab].push_back(4);
				A.piece2.tab_machines[A.piece2.index_tab].push_back(DS);
			}
		}
		else { // l'autre piece est déjà dedans
			
			// A.dpe = durée de l'assemblage + DS
			// durée de l'assemblage : suit une loi exponentielle de moyenne 10.0 (donnée en début de fonction)
			A.dpe = exp_dist(gen) + DS;

			if (A.etat == 1) { // il contient déjà la piece 1 -> donc on ajoute la piece 2
				A.piece2 = M.contenu;
				A.piece2.tab_machines[A.piece2.index_tab].push_back(4); // 4 = numero de l'assembleur
				A.piece2.tab_machines[A.piece2.index_tab].push_back(DS);
			}
			else { // il contient déjà la piece 2
				A.piece1 = M.contenu;
				A.piece1.tab_machines[A.piece1.index_tab].push_back(4); 
				A.piece1.tab_machines[A.piece1.index_tab].push_back(DS);
			}
			
			A.etat = 4; // on indique que les deux pièces sont déjà dedans
		}
		
		// on s'occupe de la machine que l'on vient de liberer

		M.etat = 0;
		M.dpe = INF;

		if (!isEmpty(f)) {
			deposer_piece_dans_la_machine(f, M, numMachine, DS);
		}
	}
}


void assembleur_vers_sortie(T_assembleur& A, int DS, T_sortie& S) {
	// maj tableau historique
	A.piece1.tab_machines[A.piece1.index_tab].push_back(DS); // sortie de l'assembleur
	A.piece1.index_tab++;
	A.piece2.tab_machines[A.piece2.index_tab].push_back(DS);
	A.piece2.index_tab++;

	// on ajoute la date de sortie du systeme sur la 1ere ligne
	A.piece1.tab_machines[0].push_back(DS);
	A.piece2.tab_machines[0].push_back(DS);

	S.traite[S.nb_traite] = A.piece1;
	S.nb_traite++;
	S.traite[S.nb_traite] = A.piece2;
	S.nb_traite++;

	A.etat = 0;
	A.dpe = INF;
}


void initElement(T_machine& MA, T_machine& MB, T_assembleur& A, T_entree& E1, T_entree& E2)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::exponential_distribution<> exp_dist(1.0 / 10.0);

	MA.duree_Traitement = 5;
	MA.etat = 0;
	MA.dpe = INF;

	MB.duree_Traitement = 1;
	MB.etat = 0;
	MB.dpe = INF;

	// Loi exponentielle pour A
	A.duree_Traitement = exp_dist(gen);
	A.etat = 0;
	A.dpe = INF;

	E1.duree_interClient = 1;
	E1.etat = 0;
	E1.dpe = 0;
	E1.id = 1;
	E1.suivant.tab_machines[0].push_back(0);
	E1.suivant.tab_machines[0].push_back(0);

	E2.duree_interClient = 10;
	E2.etat = 0;
	E2.dpe = 0;
	E2.id = 1000;
	E2.suivant.tab_machines[0].push_back(0);
	E2.suivant.tab_machines[0].push_back(0);

}


// verification de l'etat de la prochaine machine
// P a enregistre precedemment la machine ou elle veut aller ensuite
int prochain_DPE(T_machine MA, T_machine MB, T_assembleur A, T_entree E1, T_entree E2)
{
	int min_dpe = MA.dpe;
	int retour = 2;
	//  0 = Entree 1 ; 1 = Entree 2 ;  2 = A ;  3 = B ; 4 = Assembleur
	// -1 = système bloqué
	if (min_dpe > MB.dpe) {
		retour = 3;
		min_dpe = MB.dpe;
	}
	if (min_dpe > A.dpe) {
		retour = 4;
		min_dpe = A.dpe;
	}
	if (min_dpe > E1.dpe) {
		retour = 0;
		min_dpe = E1.dpe;
	}
	if (min_dpe > E2.dpe) {
		retour = 1;
		min_dpe = E2.dpe;
	}
	if (min_dpe == INF)
		retour = -1;
	return retour; 
}

void evenement_entree(int DS, T_entree& E, T_file& f, T_machine& M, int numEntree) {
	DS = E.dpe; // saut a la date du prochain evenement

	E.suivant.tab_machines[0][0] = numEntree;

	// on verifie letat de lentree
	// 1 cas : entree bloquee
	if (E.etat == 2) {
		// une place a ete liberee dans la file de A
		E.etat = 0; // pas de traitement pour l'entree
		E.suivant.tab_machines[0][1] = DS; // maj de la date d'entree dans le systeme
		E.dpe += E.duree_interClient; // temps avant que la prochaine piece ne puisse arriver

		deposer_piece_dans_la_file(f, E.suivant);
	}
	else { // 1 (evenement standard)
		E.suivant.id = E.id; // id unique par piece
		E.id++;

		E.suivant.tab_machines[0][1] = DS; // DS = date entr�e systeme

		//test A pleine
		if (isFull(f)) {
			E.etat = 2;
			E.dpe = INF;
		}
		else {
			if (M.etat == 0) {
				M.etat = 1;
				M.contenu = E.suivant;
				M.dpe = M.duree_Traitement + DS;
				M.contenu.tab_machines[M.contenu.index_tab].push_back(numEntree+2);
				M.contenu.tab_machines[M.contenu.index_tab].push_back(DS);
			}
			else {
				deposer_piece_dans_la_file(f, E.suivant);
			}
			// maj du dpe de l'entree 
			E.dpe = DS + E.duree_interClient;
		}
	}
}



