#include "Struct.h"

// Verifie si la file est vide
bool isEmpty(T_file& f) {
    return f.debut == -1;
}

// Verifie si la file est pleine
bool isFull(T_file& f) {
    return (f.debut == 0 && f.fin == TAILLE_FILE - 1) || (f.debut == f.fin + 1);
}

// Enfile un element a la fin de la file
void enfiler(T_file& f, T_piece item) {
    if (isFull(f)) {
        std::cout << "La file est pleine. Impossible d'ajouter un element." << std::endl;
    }
    else {
        if (isEmpty(f)) {
            f.debut = 0;
            f.fin = 0;
        }
        else if (f.fin == TAILLE_FILE - 1) {
            f.fin = 0;
        }
        else {
            f.fin++;
        }
        f.elements[f.fin] = item;
    }
}

// Defile un element du debut de la file
T_piece defiler(T_file& f) {
    T_piece item; // Valeur par defaut si la file est vide
    if (isEmpty(f)) {
        std::cout << "La file est vide. Impossible de retirer un element." << std::endl;
    }
    else {
        item = f.elements[f.debut];
        if (f.debut == f.fin) {
            f.debut = -1; // La file est vide apres le retrait de l'element
            f.fin = -1;
        }
        else if (f.debut == TAILLE_FILE - 1) {
            f.debut = 0;
        }
        else {
            f.debut++;
        }
    }
    return item;
}

// Initialise la file -> vide
void init_file(T_file& f) {
    f.debut = -1;
    f.fin = -1;
}
